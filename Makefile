all: engine.tar.gz engine.zip

engine.tar.gz: engine/*.py engine/README
	tar cfvz engine.tar.gz engine/*.py engine/README

engine.zip: engine/*.py engine/README
	zip engine engine/*.py engine/README
